---
layout: layout.html
summary: "Albert travels back to France."
context: "The second battle of the Somme was underway"
image: 1918/IMG_4132
group: 1918
next: 2
---

## Diary
Went to England on leave Aug 14th Returned Aug 28/18

Left Barnsley Tuesday evening 9pm August 27th, Arrived London 5.30am, went to DOVER Left D for France 2-30 pm. Arrving 4.15pm. Went to rest camp. ????? next morning, went to the Line to find my ????? , but they had moved, eventually found them on Saturday (about Dinner time), after going all over France. Sunday 1st Sept moved by train down ????? Somme, ????? about 12 hours. Plenty of work for a week whilst ????? Sunday Sept 8th. ????? went on the  