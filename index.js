var Metalsmith  = require('metalsmith');
var markdown    = require('metalsmith-markdown');
var layouts     = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');
var sass        = require('metalsmith-sass');
var serve       = require('metalsmith-serve');
var watch       = require('metalsmith-watch');


Metalsmith(__dirname)
  .metadata({
    title: "Pte. Hawcroft's Pocketbook",
    description: "My Great-Great Grandfather kept a diary in his pocket book during WWI",
    footer: "&copy; <a href='https://anthonygalvin.com/pocketbook'>Anthony Galvin</a> 2018. This content is released under the xxx license for non-commercial use"
  })
  .source('./src')
  .destination('./build')
  .clean(true)
  .use(markdown())
  .use(permalinks())
  .use(layouts({
    engine: 'handlebars'
  }))
  .use(sass({
    outputDir: 'css/'   // This changes the output dir to "build/css/" instead of "build/scss/"
  }))
  .use(watch({
      paths: {
        "${source}/**/*": true,
        "layouts/**/*": "**/*.md",
        "src/**/*": "**/*.*",
      },
      livereload: true,
    }))
  .use(serve())
  .build(function(err, files) {
    if (err) { throw err; }
  });